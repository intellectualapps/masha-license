/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.util;

/**
 *
 * @author buls
 */
public enum StatusType {
    
    PENDING("PENDING"), IN_PROCESS("IN_PROCESS"), SUCCESS("SUCCESS"), FAILED("FAILED"),
    CREATED("CREATED"), APPROVED("APPROVED"), COMPLETED("COMPLETED"), VOIDED("VOIDED"),
    CAPTURED("CAPTURED"), IN_PROGRESS("IN_PROGRESS"), CANCELED("CANCELED"), EXPIRED("EXPIRED"),
    PARTIALLY_COMPLETED("PARTIALLY_COMPLETED"), SUBMITTED("SUBMITTED"), DENIED("DENIED");
    
    String description;

    StatusType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

}

