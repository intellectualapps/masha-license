/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.intellectualapps.now.util;

import javax.xml.bind.DatatypeConverter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import javax.ejb.Stateless;
import com.intellectualapps.now.util.exception.GeneralAppException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
public class JWT {    
    SecretKey secretKey = new SecretKey();            

    public Claims parseJWT(String jwt) {
 
        Claims claims = Jwts.parser()         
           .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey.getSecret()))
           .parseClaimsJws(jwt).getBody();
        
        return claims;
    }
       
    public Claims verifyJwt(String rawToken, String resource) throws GeneralAppException {
        try {
            String authToken = rawToken.substring(7);
            return parseJWT(authToken);
        }  catch (Exception e) {
            throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, "Invalid token supplied", "Token supplied could not be parsed",
                resource);
        }
    }
    
}
