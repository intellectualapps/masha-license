/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.util;

/**
 *
 * @author buls
 */
public enum StateType {
    
    OPEN("OPEN"), ACCEPTED("ACCEPTED"), REJECTED("REJECTED"), ALL("ALL");
    
    String description;

    StateType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

}

