/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.util;

public enum BillingAgreementStatusType {
    
    PRIMARY("PRIMARY"), ACTIVE("ACTIVE"), DEACTIVATED("DEACTIVATED");
    
    String description;

    BillingAgreementStatusType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

}

