/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.util;

import java.util.logging.Logger;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */

@Stateless
public class GeneralLogger {
 
    private Logger logger = Logger.getLogger(GeneralLogger.class.getName());
    
    public GeneralLogger getLogger(String className){
        logger = Logger.getLogger(className);
        
        return this;
    }
    
    public void log(String message) {
        this.logger.info(message);
    }
    
}
