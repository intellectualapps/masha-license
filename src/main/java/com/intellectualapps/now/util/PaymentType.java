/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.util;

/**
 *
 * @author Lateefah
 */
public enum PaymentType {
    
    CREDIT_CARD("CC"), PAYPAL("PP"), NOT_AVAILABLE("NA");
    
    String description;

    PaymentType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

}

