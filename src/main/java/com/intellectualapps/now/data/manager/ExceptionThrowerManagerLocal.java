/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.data.manager;

import com.intellectualapps.now.util.exception.GeneralAppException;
/**
 *
 * @author Lateefah
 */
public interface ExceptionThrowerManagerLocal {
   
    void throwNullPreferenceAttributesException(String link) throws GeneralAppException;
        
    void throwCategoryDoesNotExistException(String link) throws GeneralAppException;
    
    void throwUserCategoryAlreadyExistException(String link) throws GeneralAppException;
    
    void throwUserCategoryDoesNotExistException (String link) throws GeneralAppException;
    
    void throwZakatGoalAlreadyExistException (String link) throws GeneralAppException;
        
    void throwZakatGoalDoesNotExistException (String link) throws GeneralAppException;
    
    void throwInvalidTokenException (String link) throws GeneralAppException;
    
    void throwInvalidDateFormatException (String link) throws GeneralAppException;
    
    void throwUserPaymentOptionAlreadyExist (String link) throws GeneralAppException;
    
    void throwUserPaymentOptionDoesNotExist (String link) throws GeneralAppException;
    
    void throwInvalidPaymentTypeException(String link) throws GeneralAppException;
    
    void throwInvalidLastFourDigitException(String link) throws GeneralAppException;
    
    void throwInvalidIsPrimaryValueException(String link) throws GeneralAppException;
    
    void throwInvalidIntegerAttributeException (String link) throws GeneralAppException;
    
    void throwInvalidDoubleAttributeException (String link) throws GeneralAppException;
    
    void throwInvalidEmailAddressException (String link) throws GeneralAppException;
    
    void throwCharityDoesNotExistException (String link) throws GeneralAppException;
    
    void throwUserCharityAlreadyExistException (String link) throws GeneralAppException;
    
    void throwSameUserProvidedException (String link) throws GeneralAppException;
    
    void throwFollowerDoesNotExistException (String link) throws GeneralAppException;
    
    void throwInvitationDoesNotExistException (String link) throws GeneralAppException;
    
    void throwInvitationAlreadyExistsException (String link) throws GeneralAppException;
    
    void throwInvalidStateTypeException(String link) throws GeneralAppException;

    void throwPayPalRestCallException (String link) throws GeneralAppException;
    
    void throwGeneralException (String link) throws GeneralAppException;
    
    void throwInvalidUserInTokenException (String link) throws GeneralAppException;

}
